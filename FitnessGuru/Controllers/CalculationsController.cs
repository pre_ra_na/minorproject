﻿using FitnessGuru.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessGuru.Controllers
{
    public class CalculationsController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();
        // GET: Calculations
        [Authorize]
        public ActionResult Calculate(UserInfo userInfo)
        {
            Calculation calc = new Calculation();
            calc.UserId = userInfo.UserId;
            calc.BMI = calc.BmiCalc(userInfo);
            calc.BMR = calc.BmrCalc(userInfo);
            calc.BMRperDay = calc.BmrPerDayCalc(userInfo, calc);
            db.Calc.Add(calc);
            db.SaveChanges();
            return View(calc);
        }


    }
}