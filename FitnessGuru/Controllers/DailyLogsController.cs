﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FitnessGuru.Models;
using Microsoft.AspNet.Identity;

namespace FitnessGuru.Controllers
{
    public class DailyLogsController : Controller
    {
        private FitnessDbCM db = new FitnessDbCM();

        // GET: DailyLogs
        public ActionResult Index()
        {
            var dailyLogs = db.DailyLogs.Include(d => d.Food).Include(d => d.Period);
            return View(dailyLogs.ToList());
        }

        // GET: DailyLogs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyLog dailyLog = db.DailyLogs.Find(id);
            if (dailyLog == null)
            {
                return HttpNotFound();
            }
            return View(dailyLog);
        }

        // GET: DailyLogs/Create
        public ActionResult Create()
        {
           ViewBag.UserId = GetCurrentUserId();
            ViewData["FoodList"] = db.Foods.Select(p => new SelectListItem() { Text = p.FoodName, Value = p.Id.ToString() }).AsEnumerable();
            ViewData["TimeList"] = db.Periods.Select(p => new SelectListItem() { Text = p.PeriodName, Value = p.TimeId.ToString() }).AsEnumerable();
            return View();
        }

        // POST: DailyLogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,UserId,TimeId,FoodId,Quantity,Calorie")] DailyLog dailyLog)
        {
            var errors = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x =>new { x.Key, x.Value.Errors }).ToArray();
            
            if (ModelState.IsValid)
            {
                dailyLog.UserId = GetCurrentUserId();
                db.DailyLogs.Add(dailyLog);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = GetCurrentUserId();
            ViewData["FoodList"] = db.Foods.Select(p => new SelectListItem() { Text = p.FoodName, Value = p.Id.ToString() }).AsEnumerable();
            ViewData["TimeList"] = db.Periods.Select(p => new SelectListItem() { Text = p.PeriodName, Value = p.TimeId.ToString() }).AsEnumerable();
            return View(dailyLog);
        }

        // GET: DailyLogs/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    DailyLog dailyLog = db.DailyLogs.Find(id);
        //    if (dailyLog == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.FoodId = new SelectList(db.Foods, "Id", "FoodName", dailyLog.FoodId);
        //    ViewBag.TimeId = new SelectList(db.Periods, "TimeId", "PeriodName", dailyLog.TimeId);
        //    return View(dailyLog);
        //}

        // POST: DailyLogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,UserId,TimeId,FoodId,Quantity,Calorie")] DailyLog dailyLog)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(dailyLog).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.FoodId = new SelectList(db.Foods, "Id", "FoodName", dailyLog.FoodId);
        //    ViewBag.TimeId = new SelectList(db.Periods, "TimeId", "PeriodName", dailyLog.TimeId);
        //    return View(dailyLog);
        //}

        // GET: DailyLogs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyLog dailyLog = db.DailyLogs.Find(id);
            if (dailyLog == null)
            {
                return HttpNotFound();
            }
            return View(dailyLog);
        }

        // POST: DailyLogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DailyLog dailyLog = db.DailyLogs.Find(id);
            db.DailyLogs.Remove(dailyLog);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public Guid GetCurrentUserId()
        { //to allow user to view only their details
            return new Guid(User.Identity.GetUserId());
        }
        public bool EnsureIsUserContact(UserInfo userInfo)
        {
            return userInfo.UserId == GetCurrentUserId();
        }
    }
}
