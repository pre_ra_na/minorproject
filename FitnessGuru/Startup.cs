﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FitnessGuru.Startup))]
namespace FitnessGuru
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
