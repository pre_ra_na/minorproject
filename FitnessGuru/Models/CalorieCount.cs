﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class CalorieCount
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public float RemainingCalorie { get; set; }
        public DateTime Date { get; set; } 

    }
}