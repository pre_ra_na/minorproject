﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FitnessGuru.Models
{
    public class UserInfoViewModel
    {
        public Guid UserId { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime date;
        public Gender Gender { get; set; }
        public DateTime Birthday { get; set; }
        public int Age { get; set; }
        public int Height { get; set; }
        public int CurrentWt { get; set; }
        public int Goalwt { get; set; }
        public IEnumerable<SelectListItem> Activeness { set; get; }
        public int ActivenessId { set; get; }
    }
    //public UserInfo User
    //{
    //    get
    //    {
    //        var tmp = new UserInfo { UserId = this.UserId}
    //    }
    //}

}