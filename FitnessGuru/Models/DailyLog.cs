﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class DailyLog
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public int TimeId { get; set; }
        [ForeignKey("TimeId")]
        public virtual TimePeriod Period { get; set; }
        public int FoodId { get; set; }
        [ForeignKey("FoodId")]
        public virtual FoodModel Food { get; set; }
        public float Quantity { get; set; }
        public float Calorie { get; set; }
        
    }

    
}