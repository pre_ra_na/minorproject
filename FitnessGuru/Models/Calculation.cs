﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FitnessGuru.Models
{
    public class Calculation
    {
        [Key]
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public double BMI { get; set; }
        public double BMR { get; set; }
        public double BMRperDay { get; set; }

        public double BmiCalc(UserInfo userInfo)
        {
            var wt = userInfo.CurrentWt;
            var ht = userInfo.Height;
           //wt = kg and ht = cm
            return (wt * 10000 / Math.Pow(ht, 2)); ;
        }
        public double BmrCalc(UserInfo userInfo)
        {
            //BMR for Men = 66.47 + (13.7 * weight [kg]) + (5 * size [cm]) − (6.8 * age [years])
            //BMR for Women = 655.1 + (9.6 * weight[kg]) + (1.8 * size[cm]) − (4.7 * age[years])
            //Harris Bennedict Formula=> revised by Mifflin

            var wt = userInfo.CurrentWt;
            var ht = userInfo.Height;
            var age = userInfo.Age;
            //wt = kg ht = cm
            if (userInfo.Gender == Gender.Male)
            {
                return ((10 * wt) + (6.25 * ht) - (5 * age) + 5);
            }
            if (userInfo.Gender == Gender.Female)
            {
                return ((10 * wt) + (6.25 * ht) - (5 * age) - 161);
            }
            return 0;

        }
        public double BmrPerDayCalc(UserInfo userInfo, Calculation calc)
        {
            /* Little to no exercise	Daily kilocalories needed = BMR x 1.2
               Light exercise (1–3 days per week)	Daily kilocalories needed = BMR x 1.375
               Moderate exercise (3–5 days per week)	Daily kilocalories needed = BMR x 1.55
               Heavy exercise (6–7 days per week)	Daily kilocalories needed = BMR x 1.725
               Very heavy exercise (twice per day, extra heavy workouts)	Daily kilocalories needed = BMR x 1.9*/

            switch (userInfo.ActivenessId)
            {
                case 1: return (calc.BMR * 1.2);
                case 2: return (calc.BMR * 1.375);
                case 3: return (calc.BMR * 1.55);
                case 4: return (calc.BMR * 1.725);
                case 5: return (calc.BMR * 1.9);
                default: return 0;

            }
        }

    }
}