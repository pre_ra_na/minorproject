namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignkey : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.DailyLogs", "FoodId");
            AddForeignKey("dbo.DailyLogs", "FoodId", "dbo.FoodModels", "FoodId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyLogs", "FoodId", "dbo.FoodModels");
            DropIndex("dbo.DailyLogs", new[] { "FoodId" });
        }
    }
}
