namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class foreignkeyT : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.DailyLogs", "TimeId");
            AddForeignKey("dbo.DailyLogs", "TimeId", "dbo.TimePeriods", "TimeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyLogs", "TimeId", "dbo.TimePeriods");
            DropIndex("dbo.DailyLogs", new[] { "TimeId" });
        }
    }
}
