namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dailylogadded : DbMigration
    {
        public override void Up()
        {
           
            CreateTable(
                "dbo.CalorieCounts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RemainingCalorie = c.Single(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DailyLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        TimeId = c.Int(nullable: false),
                        FoodId = c.Int(nullable: false),
                        Quantity = c.Single(nullable: false),
                        Calorie = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TimePeriods",
                c => new
                    {
                        TimeId = c.Int(nullable: false, identity: true),
                        PeriodName = c.String(),
                    })
                .PrimaryKey(t => t.TimeId);
            
           
        }
        
        public override void Down()
        {
           
           
            DropTable("dbo.TimePeriods");
            DropTable("dbo.DailyLogs");
            DropTable("dbo.CalorieCounts");
            AddPrimaryKey("dbo.FoodModels", "Id");
        }
    }
}
