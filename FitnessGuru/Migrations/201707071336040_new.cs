namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _new : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Calculations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        BMI = c.Double(nullable: false),
                        BMR = c.Double(nullable: false),
                        BMRperDay = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.FoodModels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FoodName = c.String(),
                        ServingSize = c.String(),
                        Calories = c.Single(nullable: false),
                        Carbohydrate = c.Single(nullable: false),
                        Fat = c.Single(nullable: false),
                        Protein = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Guid(nullable: false),
                        FName = c.String(),
                        LName = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                        RegisteredDate = c.DateTime(nullable: false),
                        Gender = c.Int(nullable: false),
                        Birthday = c.DateTime(nullable: false),
                        Age = c.Int(nullable: false),
                        Height = c.Int(nullable: false),
                        CurrentWt = c.Int(nullable: false),
                        Goalwt = c.Int(nullable: false),
                        ActivenessId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserInfoes");
            DropTable("dbo.FoodModels");
            DropTable("dbo.Calculations");
        }
    }
}
