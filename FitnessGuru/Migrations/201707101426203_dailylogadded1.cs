namespace FitnessGuru.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dailylogadded1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.FoodModels", name: "Id", newName: "FoodId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.FoodModels", name: "FoodId", newName: "Id");
        }
    }
}
